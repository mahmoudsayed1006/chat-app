import express from 'express';
import FriendController from '../../controllers/friends/friends.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:userId/friends')
    .get(FriendController.findAll);
    
router.route('/:friendId/unFriend')
    .put( requireAuth,FriendController.unFriend);







export default router;