import express from 'express';
import { requireSignIn, requireAuth} from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);
router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedBody(true),
    UserController.updateInfo);

router.route('/getAll')
    .get(UserController.findAll)

router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete)
export default router;
