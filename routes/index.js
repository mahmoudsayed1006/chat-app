import express from 'express';
import userRoute from './user/user.route';
import messageRoute  from './message/message.route';
import friendRoute  from './friends/friends.route';

const router = express.Router();

router.use('/', userRoute);
router.use('/messages',messageRoute);
router.use('/friends',friendRoute);
export default router;
