import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Message from "../../models/message/message.model";
import ApiError from '../../helpers/ApiError';
import { handleImg} from "../shared/shared.controller";
import { isInArray } from "../../helpers/CheckMethods";
import i18n from "i18n";
import { transformMessage } from "../../models/message/transformMessage";

var messageController = {
    async uploadImage(req, res, next) {
        try {
            let image = await handleImg(req);
            res.send({image});
            } catch (error) {
            next(error)
        }
    },
    /**
     * send message in private chat
     * @param {*} io 
     * @param {*} nsp 
     * @param {toId,fromId,dataType,duration,content,secretKey,preview} data 
     */
    async addnewMessage(io, nsp, data) {
        logger.info(`add message socket data  : ${JSON.stringify(data)}`);
        var toRoom = 'room-' + data.toId; 
        var fromRoom = 'room-' + data.fromId;

        logger.info(`new message to room ${toRoom}`);
        let now = new Date();
        var messData = { //data 
            to: data.toId,
            from: data.fromId,
            sent: true,
            lastMessage: true,
        }
        
        if (data.dataType != null) {
            messData.content = data.content;
            messData.dataType = data.dataType;
        }
        if(data.duration != null) {
            messData.duration = data.duration;
        }
        if(data.secretKey != null) {
            messData.secretKey = data.secretKey;
        }
        //query to get chat data
        var query1 = { 
            to: data.toId,
            from: data.fromId,
            lastMessage: true,
            deleted: false
        }
        var query2 = { 
            to: data.fromId,
            from: data.toId,
            lastMessage: true,
            deleted: false
        }
        //get unread messages counts
        var countquery = {
            to : data.toId , 
            deleted : false , 
            seen : false 
        }
        var Count = await Message.countDocuments(countquery);
        Count = Count + 1 ;
        Message.updateMany({ $or: [query1, query2] }, { lastMessage: false })
            .then((result1) => {
                // old v2  is  io.nsps['/chat'].adapter.rooms
                if (io.sockets.adapter.rooms[toRoom]) { //room is open 
                    messData.delivered = true;
                }
                var message = new Message(messData);
                logger.info(`messData ${JSON.stringify(messData)}`);
                message.save()
                    .then(async(result2) => {
                        logger.info(`messData ${JSON.stringify(data)}`);
                        let theMessage = await Message.findById(result2._id).populate('from to')
                        let msg = await transformMessage(theMessage)
                        nsp.to(toRoom).emit('newMessage', {data:msg,count : Count});
                        notificationNSP.to(toRoom).emit('updateUnInformedMessage',{count : Count});

                        nsp.to(fromRoom).emit('newMessage', {data:msg});
                        if (io.sockets.adapter.rooms[toRoom]){
                            logger.info(`friend is online`);
                            nsp.to(fromRoom).emit('delivered', { friendId: data.toId });
                        }
                        
                    })
                    .catch(err => {
                        logger.error(`can not save the message : ${JSON.stringify(err)}`);
                        console.log(err);
                    });
            }).catch((err) => {
                logger.error(`can not update Last Message : ${JSON.stringify(err)}`);
                console.log(err);
            });
    },
    /**
     * get chat bettween two users with pagenation
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     * @returns {"success": true,"data": [],"page": 1,"pageCount": 0,"limit": 20,"totalCount": 0,"links":{}}
     */
    async getAllMessages(req, res, next) {
        let page = +req.query.page || 1, limit = +req.query.limit || 20;
        let {userId, friendId,out} = req.query;
        //user try to get other users chat
        if(!isInArray(["ADMIN","SUB-ADMIN"],req.user.type)){
            if(req.user._id != userId && req.user._id != friendId)
                return next(new ApiError(403, i18n.__('admin.auth')));
        }
        
        var query1 = {deleted: false };
        var query2 = {deleted: false}
        if (userId) {
            query1.to= userId;
            query2.from= userId;
        }
        if (friendId) {
            query1.from= friendId;
            query2.to= friendId;
        }
        
        Message.find({ $or: [query1, query2] })
            .limit(limit)
            .skip((page - 1) * limit)
            .populate(populateQuery)
            .sort({ _id: -1 })
            .then(async(data) => {
                let newdata = []
                await Promise.all(data.map(async(e)=>{
                    let index = await transformMessage(e)
                    newdata.push(index);
                }));
                const count = await Message.countDocuments({ $or: [query1, query2] });
                const pageCount = Math.ceil(count / limit);
                res.send(new ApiResponse(newdata, page, pageCount, limit, count, req));
            })
            .catch(err => {
                next(err);
            });
            
    },
    /**
     * get unseen Count
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     * @returns {success:true,unseen:number}
     */ 
    async unseenCount(req, res, next) {
        try {
            let user = req.user._id;
            let query = { deleted: false,to:user,seen:false };
            const unseenCount = await Message.countDocuments(query);
            res.status(200).send({
                unseen:unseenCount,
            });
        } catch (err) {
            next(err);
        }
    },
    /**
     * update Seen bettween two users
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     * @returns 
     */
    updateSeen(req, res, next) {
        var myId = +req.query.userId || 0;
        var friendId = +req.query.friendId || 0;
        //user try to get other users chat
        if(!isInArray(["ADMIN","SUB-ADMIN"],req.user.type)){
            if(req.user._id != myId && req.user._id != friendId)
                return next(new ApiError(403, i18n.__('admin.auth')));
        }
        var toRoom = 'room-' + friendId;
        var query1 = {
            to: myId,
            from: friendId,
            seen: false
        };
        Message.updateMany(query1, { seen: true,informed:true, seenDate: Date.now() })
            .exec()
            .then(async(data) => {
                var countquery = {
                    to : myId , 
                    deleted : false , 
                    informed : false 
                }
                var Count = await Message.countDocuments(countquery);
                //emit to update message informed
                notificationNSP.to(toRoom).emit('updateUnInformedMessage',{count : Count});
                res.status(200).send('Updated.');
            })
            .catch((err) => {
                next(err);
            });
    },
    /**
     * update Seen Socket bettween two users
     * @param {*} nsp 
     * @param {*} data 
     */
    updateSeenSocket(nsp, data) { 
        var myId = data.myId || 0;
        var friendId = data.friendId || 0;
        var toRoom = 'room-' + friendId;
        var fromRoom = 'room-' + myId;
        var query1 = {
            to: myId,
            from: friendId,
            seen: false
        };
        Message.updateMany(query1, { seen: true, informed:true , seenDate: Date.now() })
            .exec()
            .then(async(result) => {
                 var countquery = {
                    to : myId , 
                    deleted : false , 
                    informed : false 
                }
                var Count = await Message.countDocuments(countquery);
               
                nsp.to(toRoom).emit('seen', { success: true ,unseenCount:Count});
                nsp.to(fromRoom).emit('seen', { success: true ,unseenCount:Count});
            })
            .catch((err) => {
                console.log(err);
            });
    },
    /**
     * get last contacts with pagenation
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     * @returns {"success": true,"data": [],"page": 1,"pageCount": 0,"limit": 20,"totalCount": 0,"links":{}}
     */
    async findLastContacts(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { id } = req.query;
            let query1 = { deleted: false ,lastMessage: true };
            if (id) query1.to = id;
            let query2 = { deleted: false , lastMessage: true };
            if (id) query2.from = id;
            //user try to get other users chat
            if(!isInArray(["ADMIN","SUB-ADMIN"],req.user.type)){
                if(req.user._id != id)
                    return next(new ApiError(403, i18n.__('admin.auth')));
            }

            await Message.find({ $or: [query1, query2] })
                .sort({ _id: -1 })
                .skip((page - 1) * limit)
                .limit(limit)
                .populate(populateQuery)
                .then(async (data) => {
                    let newdata = [];
                    await Promise.all(data.map(async (e) => {
                        let queryCount = {
                            deleted: false,
                            to: req.user._id,
                            seen: false,
                        }
                        //check if from equal to id 
                        if (e.from._id === id) {
                            queryCount.from = e.to._id;
                        } else {
                            queryCount.from = e.from._id;
                        }

                        let index = await transformMessage(e)
                        let unseenCount = await Message.countDocuments(queryCount);
                        index.unseenCount = unseenCount
                        newdata.push(index);
                    }));
                    newdata = newdata.sort((a, b) => b.id - a.id);

                    const messagesCount = await Message.find({ $or: [query1, query2] }).countDocuments();
                    const pageCount = Math.ceil(messagesCount / limit);
                    res.send(new ApiResponse(newdata, page, pageCount, limit, messagesCount, req));
                })

        } catch (err) {
            next(err);
        };
    },
    /**
     * change user status
     * @param {*} socket 
     * @param {userId} data 
     * @param {*} check 
     */
    changeStatus(socket,data ,check){
        var userId = data.userId;
        User.findByIdAndUpdate(userId,{status:check},{new: true})
        .then((data1)=>{
            if(check){
                socket.broadcast.emit('online',data1);
            }
            else{
                socket.broadcast.emit('offline',data1);
            }
        })
        .catch((err)=>{
            console.log(err);
        });
    },
    

    
};

module.exports = messageController;
