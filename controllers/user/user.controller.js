import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator';
import { checkValidations, handleImg } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import ApiError from '../../helpers/ApiError';
import ApiResponse from "../../helpers/ApiResponse";
import bcrypt from 'bcryptjs';

export default {
    async signIn(req, res, next) {
        try{
            
            let user = req.user;
            user = await User.findById(user.id);
            if(!user)
                return next(new ApiError(403, ('phone or password incorrect')));
           
            if(user.deleted == true){
                return next(new ApiError(403, ('sorry you are deleted')));
            }
            res.status(200).send({
                success: true,
                data:user,
                token: generateToken(user.id)
            });
        } catch(err){
            next(err);
        }
    },
    

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('fullname').not().isEmpty().withMessage('fullname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img'),

        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required')
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            
            if (req.file) {
               let image = await handleImg(req)
               validatedBody.img = image;
            }
            let createdUser = await User.create({
                ...validatedBody
            });
            res.status(201).send({
                success: true,
                data: await User.findOne(createdUser),
                token: generateToken(createdUser.id)
            });
            

        } catch (err) {
            next(err);
        }
    },
    
   
    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id);
            res.send({ success: true, data:user});
        } catch (error) {
            next(error);
        }
    },

    validateUpdatedBody(isUpdate = true) {
        let validations = [
            body('fullname').optional(),
            body('phone').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { phone: value,deleted:false };
                    if (isUpdate && user.phone === value)
                        userQuery._id = { $ne: userId };
                    
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
            }),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },
    async updateInfo(req, res, next) {
        try {
            let {userId} = req.params;
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            if(validatedBody.fullname){
                user.fullname = validatedBody.fullname;
            }
            if(validatedBody.phone){
                user.phone = validatedBody.phone;
            }
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
           
            await user.save();
            res.status(200).send({
                success: true,
                data: await User.findById(userId)
            });

        } catch (error) {
            next(error);
        }
    }, 
    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                success: true,
                data: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
   
    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {type } = req.query;
            let query = {deleted: false };
            if (type)
                query.type = type;
            let users = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);

            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let { userId } = req.params;
            let user = await checkExistThenGet(userId, User);
            user.deleted = true;
            await user.save();
            res.status(200).send({ success: true});
        } catch (error) {
            next(error)
        }
    },
   


};
