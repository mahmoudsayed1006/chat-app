import { checkExistThenGet } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Friend from "../../models/friends/friends.model";
import ApiError from '../../helpers/ApiError';
const populateQuery = [
    {path:'friend',modal:"user"},
    {path:'user',modal:"user"},
]
var friendsController = {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { userId } = req.params;
            let query = {
                $and: [
                    { $or: [
                        {user: userId}, 
                        {friend: userId}, 
                      ] 
                    },
                    {deleted: false},
                ]
            };
            let Friends = await Friend.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const FriendsCount = await Friend.count(query);
            const pageCount = Math.ceil(FriendsCount / limit);

            res.send(new ApiResponse(Friends, page, pageCount, limit, FriendsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async addFriend(nsp, data) {
        console.log(data);
        var toRoom = 'room-' + data.friendId; 
        var fromRoom = 'room-' + data.myId;
        var FriendData = { 
            user: data.myId,
            friend: data.friendId,
          
        }
        var query1 = {deleted: false ,user: data.myId, friend: data.friendId};
        var query2 = {deleted: false ,user: data.friendId, friend: data.myId}

        if (await Friend.findOne({ $or: [query1, query2] })){
            nsp.to(fromRoom).emit('addFriend', {error:'you already Friend this user'});   
        } else{ 
            let friend = await checkExistThenGet(data.friendId, User);
            friend.friends.push(data.myId);
            await friend.save();
            let myUser = await checkExistThenGet(data.myId, User);
            myUser.friends.push(data.friendId);
            await myUser.save();
            var theFriend = new Friend(FriendData);    
            theFriend.save()
            .then(async (data1) => {
                nsp.to(toRoom).emit('addFriend', {data:data1,user:myUser});     
            })
            .catch((err)=>{
                console.log(err)
            });
        }
       
    },
    async unFriend(req, res, next) {
        try {
            let {friendId } = req.params;
            var query1 = {deleted: false ,user: data.myId, friend: data.friendId};
            var query2 = {deleted: false ,user: data.friendId, friend: data.myId}
            let friends = await Friend.findOne({ $or: [query1, query2] })

            if (await Friend.findOne({ $or: [query1, query2] })){
                return next(new ApiError(500, ('removed succes')));
            }
            let friend = await checkExistThenGet(friends.id, Friend, { deleted: false });
            console.log(friend)
            friend.deleted = true;
            await friend.save();
            let user = await checkExistThenGet(friendId, User);
            let arr = user.friends;
            let myId = req.user._id;
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == myId){
                    arr.splice(i, 1);
                }
            }
            
            user.friends = arr;
            await user.save();

            
            let myuser = await checkExistThenGet(req.user._id, User);
            
            let arr2 = myuser.friends;
            console.log(arr2);
            for(let i = 0;i<= arr2.length;i=i+1){
                if(arr2[i] == friendId){
                    arr2.splice(i, 1);
                }
            }
            console.log(arr2);
            myuser.friends = arr2;
            await myuser.save();
            myuser = await User.findById(req.user._id);
            res.send({myuser});
        } catch (error) {
            next(error)
        }
    },

}
module.exports = friendsController;