var MessageController = require('../controllers/message/messageController');
var FriendController = require('../controllers/friends/friends.controller');

module.exports = {

    startChat: function (io) {  
        console.log('socket is on')
        var nsp = io.of('/chat'); //namespace

        nsp.on('connection', (socket) => { //connect to socket
            var myId = socket.handshake.query.id; 
           
            var roomName = 'room-' + myId; 
            socket.join(roomName); 
            console.log('client ' + myId + ' connected.');

            MessageController.getMyInfo(socket,{id: myId});
            
            var clients1 = nsp.allSockets();   
            socket.userId = myId; 
            console.log("socket: "+socket.userId);
            var clients=[];
            for (var id in clients1.connected) { 
                var userid= clients1.connected[id].userId;
                clients.push(userid);
            }
            socket.on('newMessage', function (data) { 
                console.log(data);
                MessageController.addnewMessage(io,nsp,data);
            });
            socket.on('typing', function (data) { 
                var toRoom = 'room-' + data.toId;
                nsp.to(toRoom).emit('typing', data);
            });


            socket.on('stopTyping', function (data) {
                var toRoom = 'room-' + data.toId;
                nsp.to(toRoom).emit('stopTyping', data);
            });
            socket.on('seen',function(data){
                data.myId = myId;
                console.log("in server in seeen")
                MessageController.updateSeenSocket(nsp,data);

            });
            socket.on('online',function(){
                var check = true; 
                MessageController.changeStatus(socket,{id: myId},check);
                console.log('user is online')
            });

            socket.on('offline',function(){
                var check = false;
                MessageController.changeStatus(socket,{id: myId},check);
                console.log('user is offline')
            });

            socket.on('addFriend', function (data) { 
                console.log(data);
                FriendController.addFriend(nsp,data);
            });

           
            socket.on('disconnect', function () {
                var check = false;
                console.log('client disconnected');
                MessageController.changeStatus(socket,{id: myId},check);
                nsp.emit('clientDisconnected',{id: myId})
            });
            

        });
    },
}