
export async function transformMessage(e,lang) {
    let index = {
        dataType: e.dataType,
        createdAt: e.incommingDate,
        duration: e.duration,
        sent:e.sent,
        delivered:e.delivered,
        seen: e.seen,
        seenDate:e.seenDate,
        secretKey:e.secretKey,
        content:e.content,
        createdAt:e.createdAt,
        id:e._id
    }
    if(e.from){
        let from = {
            fullname:e.from.fullname?e.from.fullname:"",
            phone:e.from.phone,
            img:e.from.img,
            id:e.from._id
        }
        index.from = from
    }
    if(e.to){
        let to = {
            fullname:e.to.fullname?e.to.fullname:"",
            phone:e.to.phone,
            img:e.to.img,
            id:e.to._id
        }
        index.to = to
    }
    
    return index;
}
