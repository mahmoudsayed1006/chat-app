import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
const friendSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    friend: {
        type: Number,
        ref: 'user',
    },
    user: {
        type: Number,
        ref: 'user'
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });
friendSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
friendSchema.plugin(autoIncrement.plugin, { model: 'friend', startAt: 1 });
export default mongoose.model('friend', friendSchema);
