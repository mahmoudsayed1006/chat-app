import config from '../config';
import ApiError from '../helpers/ApiError';
import * as cloudinary from 'cloudinary';

export * from './token';

cloudinary.config(config.cloudinary);

// Convert Local Upload To Cloudinary Url  toImgUrl
export async function toImgUrl (multerObject) {
  try {
    let result = await cloudinary.v2.uploader.upload(multerObject.path);
    return result.secure_url;
  }
  catch (err) {
    console.log('Cloudinary Error: ', err);
    throw new ApiError(500, 'Failed To Upload An Image due to network issue! Retry again...');
  }
}


// Convert Local Upload To Full Url  toImgUrlCloudinary
export async function toImgUrlCloudinary(multerObject) {

  return `${config.appUrl}/${multerObject.destination}/${multerObject.filename}`;

}